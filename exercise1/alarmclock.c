#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#define TIME_FORMAT "%Y-%m-%d %H:%M:%S"
#define MAX_ALARMS 10

void get_cmd(void);
void print_time(struct tm* time_ptr);
void schedule_alarm(void);
void list_alarms(void);
void cancel_alarm(void);
void remove_saved_alarm(int);

struct alarm {
    time_t timestamp;
    int pid;
};
struct alarm alarms[MAX_ALARMS];
int alarm_index = 0;

int main() {
    struct tm* now_tm = localtime(&(time_t){time(NULL)});
    printf("Welcome to the alarm clock! It is currently ");
    print_time(now_tm);
    printf("\nPlease enter \"s\" (schedule), \"l\" (list), \"c\" (cancel), \"x\" (exit)\n");

    while (1) {
        get_cmd();
    }

	return 0;
}

void get_cmd() {
    char cmd, c;
    printf("> ");
    // scanf(" %c", &cmd);
    do {
        cmd = c;
        c = getchar();
    } while (c != '\n' && c != EOF);

    switch (cmd) {
        case 's':
            schedule_alarm();
            break;

        case 'l':
            list_alarms();
            break;

        case 'c':
            cancel_alarm();
            break;

        case 'x':
            printf("Goodbye!\n");
            exit(EXIT_SUCCESS);
            break;
        
        default:
            printf("Unknown command!\n\n");
            break;
    }
}

void print_time(struct tm* time_ptr) {
    char buf[100];
    strftime(buf, sizeof(buf), TIME_FORMAT, time_ptr);
    printf("%s", &buf);
}

void schedule_alarm() {
    char date_str[100];
    struct tm alarm_tm;

    if (alarm_index >= MAX_ALARMS) {
        printf("Failed to set alarm! Maximum number of alarms reached.\n\n");
        return;
    }

    printf("Schedule alarm at which date and time? ");
    fgets(date_str, sizeof(date_str), stdin);

    if (strptime(date_str, TIME_FORMAT, &alarm_tm) == NULL) {
        printf("Failed to set alarm! Invalid date and time input.\n\n");
    } else {
        alarm_tm.tm_isdst = -1; // ignore daylight saving
        time_t alarm_t = mktime(&alarm_tm);

        long seconds = alarm_t - time(NULL);
        if (seconds < 0) {
            printf("Failed to set alarm! Specified date is in the past.\n\n");
            return;
        }
        printf("Scheduling alarm in %d seconds\n", seconds);

        int pid = fork();
        if (pid == 0) {
            // child process
            sleep(seconds);
            execlp("mpg123", "mpg123", "-q", "alarm.mp3", (char *) NULL);
            exit(EXIT_SUCCESS);
        }
        printf("Child process created with pid %d\n", pid);

        struct alarm new_alarm = {
            .timestamp = alarm_t,
            .pid = pid
        };
        alarms[alarm_index++] = new_alarm;
    }
}

void list_alarms() {
    for (int i = 0; i < MAX_ALARMS; i++) {
        if (alarms[i].pid != 0) {
            printf("Alarm %d at ", i+1);
            struct tm* time_tm = localtime(&alarms[i].timestamp);
            print_time(time_tm);
            printf("\n");
        }
    }
}

void cancel_alarm() {
    int pid;
    char pid_str[5];

    printf("Cancel which alarm? ");
    fgets(pid_str, sizeof(pid_str), stdin);
    pid = atoi(pid_str);

    if (pid <= 0 || pid > MAX_ALARMS) {
        printf("Failed to cancel alarm! Invalid input.\n\n");
        return;
    }
    
    int index = pid-1;
    struct alarm to_cancel = alarms[index];
    if (to_cancel.pid == 0) {
        printf("Failed to cancel alarm! Did not find specified alarm %d.\n\n", pid);
        return;
    }

    kill(to_cancel.pid, SIGKILL);
    remove_saved_alarm(index);
}

void remove_saved_alarm(int index) {
    // remove from alarms array
    for (int i = index; i < MAX_ALARMS - 1; i++)
        alarms[i] = alarms[i+1];
    
    struct alarm empty = {
        .pid = 0,
        .timestamp = 0
    };
    alarms[MAX_ALARMS - 1] = empty;
}
